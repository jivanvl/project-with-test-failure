require 'spec_helper'
require './demo'

RSpec.describe Demo do
  context '#hello_world' do
    it 'should return a hello world message' do
      demo = Demo.new

      expect(demo.hello_word).to eq 'Hello, World!'
    end
  end
end
